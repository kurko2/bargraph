#include <QDebug>
#include <QRegularExpression>
#include <QMap>
#include <QQmlPropertyMap>
#include <QGuiApplication>
#include <QQmlApplicationEngine>
#include <QQmlContext>
#include "qttricks/qqmlhelpers.h"

#include "GraphModel.h"
#include "SortAlphabetModel.h"

int main(int argc, char *argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setOrganizationName(QStringLiteral("Krasnoarmeyskaya 129"));
    QCoreApplication::setOrganizationDomain(QStringLiteral("https://yandex.ru/"));
    QCoreApplication::setApplicationName(QStringLiteral("GraphBar"));

    QGuiApplication app(argc, argv);

    QQmlApplicationEngine engine;
    const QUrl url(QStringLiteral("qrc:/main.qml"));
    QObject::connect(&engine, &QQmlApplicationEngine::objectCreated,
                     &app, [url](QObject *obj, const QUrl &objUrl) {
        if (!obj && url == objUrl)
            QCoreApplication::exit(-1);
    }, Qt::QueuedConnection);


    engine.rootContext()->setContextProperty("graphModel", GraphModel::getInstance(qobject_cast<QObject *>(&app)));
    engine.rootContext()->setContextProperty("sortAlphabetModel", SortAlphabetModel::getInstance(qobject_cast<QObject *>(&app)));
    SortAlphabetModel::getInstance()->setSourceModel(GraphModel::getInstance());
    SortAlphabetModel::getInstance()->sort(0);
    engine.load(url);

    return app.exec();
}
