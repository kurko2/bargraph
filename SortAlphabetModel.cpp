#include <QDebug>
#include "SortAlphabetModel.h"
#include "GraphModel.h"

SortAlphabetModel::SortAlphabetModel(QObject *parent) :
   QSortFilterProxyModel(parent)
{
}

bool SortAlphabetModel::lessThan(const QModelIndex &left, const QModelIndex &right) const
{
    return sourceModel()->data(left, GraphModel::wordRole).toString()
            < sourceModel()->data(right, GraphModel::wordRole).toString();
}
