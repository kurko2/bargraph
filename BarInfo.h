#ifndef BARINFO_H
#define BARINFO_H

#include <qglobal.h>
#include <QString>
#include <QColor>
#include <QRandomGenerator>

struct BarInfo {
    BarInfo(const QString & name, const qint32 count) :
        m_word(name),
        m_count(count),
        m_color(/*QColor::fromRgb(QRandomGenerator::global()->generate())*/QColor::fromRgb(0x556677))
    {  }

    QString m_word;
    qint32 m_count;
    QColor m_color;
};

#endif // BARINFO_H
