import QtQuick 2.14
import QtQuick.Window 2.14
import QtQuick.Controls 2.14
import QtQuick.Layouts 1.14
import QtQuick.Dialogs 1.2

Window {
    visible: true
    width: 640
    height: 520
    title: qsTr("Bar Graph")

    FileDialog {
        id: loadDialog
        modality: Qt.NonModal
        selectExisting: true
        nameFilters: ["TXT files (*.txt)"]
        onAccepted: {
            graphModel.openFile(loadDialog.fileUrl);
            openButton.enabled = false
        }
    }

    ColumnLayout {

        anchors.fill: parent
        anchors.margins: 24

        ProgressBar {
            to: graphModel.commonSymbol
            from: 0
            value: graphModel.currentSymbol
            Layout.fillWidth: true
            Layout.preferredHeight: 20
        }

        ListView {
            id: listView
            clip: true
            Layout.fillHeight: true
            Layout.fillWidth: true
            spacing: 1

            model: sortAlphabetModel
            delegate: Rectangle {
                width: listView.width
                height: 24

                Row {
                    anchors.fill: parent

                    Label {
                        id: label
                        anchors.verticalCenter: parent.verticalCenter
                        width: 100
                        text: model.barWord
                    }

                    ProgressBar {
                        id: control
                        width: parent.width - label.width
                        height: parent.height
                        from: 0
                        to: graphModel.maxCount // graphModel.wordsAmount
                        value: model.barCount

                        background: Rectangle {
                            color: "transparent"
                        }

                        contentItem: Item {
                            Rectangle {
                                width: control.visualPosition * parent.width
                                height: parent.height
                                color: model.barColor

                                Text {
                                    anchors.verticalCenter: parent.verticalCenter
                                    text: model.barCount
                                }
                            }
                        }
                    }
                }
            }
        }

        Button {
            id: openButton
            text: qsTr("Открыть файл")
            Layout.alignment: Qt.AlignCenter
            onClicked:{
                loadDialog.open()
            }
            Connections {
                target: graphModel
                function onDoFinishParsing() { openButton.enabled = true }

            }
        }

        Label {
            Layout.alignment: Qt.AlignLeft
            text: qsTr("Всего обработано слов: %1").arg(graphModel.wordsAmount)
        }
    }

}
