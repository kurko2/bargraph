#include <QDebug>
#include <QCoreApplication>
#include <QColor>
#include <QUrl>
#include "GraphModel.h"
#include "SortAlphabetModel.h"

GraphModel::GraphModel(QObject *parent) :
    QAbstractListModel(parent),
    m_wordsAmount(0),
    m_maxCount(0),
    m_parser(new Parser())
{
    m_parser->moveToThread(&m_thread);
    connect(this, &GraphModel::doParse, m_parser.data(), &Parser::parse, Qt::QueuedConnection);
    connect(m_parser.data(), &Parser::doUpdateModel, this, &GraphModel::onUpdateModel, Qt::QueuedConnection);
    connect(m_parser.data(), &Parser::doFinishParsing, this, &GraphModel::doFinishParsing);

    connect(m_parser.data(), &Parser::alive, this, [this](int max, int c){
        update_commonSymbol(max);
        update_currentSymbol(c);
    }, Qt::QueuedConnection);

    m_thread.start();
}

GraphModel::~GraphModel()
{
    //! TODO: добавить в Parser atomicBool и сделать человеческое закрытие потока
}

int GraphModel::rowCount(const QModelIndex &parent) const
{
    Q_UNUSED(parent)
    return m_rows.size();
}

QHash<int, QByteArray> GraphModel::roleNames() const
{
    static const QHash<int, QByteArray> roles
            ({
                 {wordRole , "barWord"},
                 {countRole, "barCount"},
                 {colorRole, "barColor"}
             });
    return roles;
}

QVariant GraphModel::data(const QModelIndex &index, int role) const
{
    if ((!index.isValid()) || (index.row() >= static_cast<int>(m_rows.size())))
        return QVariant();

    switch(role)
    {
    case wordRole:
        return  m_rows.at(index.row())->m_word;
    case countRole:
        return  m_rows.at(index.row())->m_count;
    case colorRole:
        return m_rows.at(index.row())->m_color;

    default:
        return QVariant();
    }
}

void GraphModel::openFile(const QUrl &fileName)
{
    m_maxCount = 0;
    m_wordsAmount = 0;
    beginResetModel();
    m_rows.clear();
    endResetModel();

    emit doParse(fileName.toLocalFile());
}

void GraphModel::onUpdateModel(const std::vector<QPair<QString, qint32> > &sorted, const qint32 wordsCount)
{
    update_maxCount(sorted.front().second);
    update_wordsAmount(wordsCount);

    //! TODO: ну добавить-таки поиск существующих слов и заменить на dataChanged()
    beginResetModel();
    m_rows.clear();
    for (auto iter = sorted.cbegin(); iter < sorted.cend(); ++iter) {
        //m_rows.push_back(QSharedPointer<BarInfo>(new BarInfo((*iter).first, (*iter).second)));
        m_rows.emplace_back(new BarInfo((*iter).first, (*iter).second));
    }

    endResetModel();
}
