#ifndef SORTALPHABETMODEL_H
#define SORTALPHABETMODEL_H

#include <QObject>
#include <QSortFilterProxyModel>
#include "qttricks/qqmlhelpers.h"

class SortAlphabetModel : public QSortFilterProxyModel
{
    Q_OBJECT
    SINGLETON(SortAlphabetModel)

public:
    SortAlphabetModel(QObject * parent);

private:
    bool lessThan(const QModelIndex &left, const QModelIndex &right) const override;

};

#endif // SORTALPHABETMODEL_H
