#include <QDebug>
#include <QThread>
#include <QFile>
#include <QRegularExpression>
#include <QCoreApplication>
#include "Parser.h"

Parser::Parser(QObject *parent) : QObject(parent)
{

}

void Parser::parse(const QString &fileName)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
        return;

    QString letter(file.readAll());
    file.close();
    letter = letter.toLower();
    letter.remove(QRegularExpression("[^А-Яа-яA-Za-z0-9_\\s]"));

    m_rows.clear();

    int idTimer = startTimer(200);

    QString word;
    int i = 0;
    for (const auto & symbol : letter) {
        ++i;
        if (!symbol.isSpace()) {
            word.append(symbol);
        } else {
            if (!word.isEmpty()) {
                insert(word);
                word.clear();
            }
        }

        emit alive(letter.size(), i);
    }

    emit alive(letter.size(), i);
    emit doFinishParsing();
    killTimer(idTimer);

    sorting();
}

void Parser::insert(const QString &word)
{
    auto i = m_rows2Hash.constFind(word);
    if (i != m_rows2Hash.constEnd()) {
        m_rows2[i.value()].second++;
    } else {
        m_rows2Hash[word] = m_rows2.size();
        //m_rows2.push_back(QPair<QString, qint32>(word, 1));
        m_rows2.emplace_back(word, 1);
    }

    QCoreApplication::processEvents();
}

void Parser::timerEvent(QTimerEvent *event)
{
    Q_UNUSED(event)

    if (m_rows2.size() > TOP15) {
        sorting();
    }
}
