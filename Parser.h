#ifndef PARSER_H
#define PARSER_H

#include <QObject>
#include <QHash>
#include <QSharedPointer>
#include "BarInfo.h"

#define TOP15   15

class Parser : public QObject
{
    Q_OBJECT
public:
    explicit Parser(QObject *parent = nullptr);

signals:
    void doUpdate(const QString & word, const qint32 count);
    void doFinishParsing();
    void alive(int max, int c);

    void doUpdateModel(const std::vector<QPair<QString, qint32>> &sorted, const qint32 wordsCount);

public slots:
    void parse(const QString & fileName);

private:
    QHash<QString, qint32> m_rows;

    std::vector<QPair<QString, qint32>> m_rows2;
    QHash<QString, qint32> m_rows2Hash;

    std::vector<QPair<QString, qint32>> m_sorted{TOP15};

    void insert(const QString &word);
    void timerEvent(QTimerEvent *event) override;
    void sorting()
    {
        std::partial_sort_copy(m_rows2.cbegin(), m_rows2.cend(), m_sorted.begin(), m_sorted.end(), [](const QPair<QString, qint32> & a, const QPair<QString, qint32> & b){
            return a.second > b.second;
        });

        emit doUpdateModel(m_sorted, m_rows2.size());
    }
};

#endif // PARSER_H
