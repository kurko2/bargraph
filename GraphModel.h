#ifndef GRAPHMODEL_H
#define GRAPHMODEL_H

#include <QObject>
#include <QAbstractListModel>
#include <QSharedPointer>
#include <QScopedPointer>
#include <QQmlPropertyMap>
#include <QThread>
#include <queue>

#include "Parser.h"
#include "BarInfo.h"
#include "qttricks/qqmlhelpers.h"


class GraphModel : public QAbstractListModel
{
    Q_OBJECT
    SINGLETON(GraphModel)
    QML_READONLY_PROPERTY(qint32, wordsAmount)
    QML_READONLY_PROPERTY(qint32, maxCount)

    QML_READONLY_PROPERTY(qint32, commonSymbol)
    QML_READONLY_PROPERTY(qint32, currentSymbol)


public:
    explicit GraphModel(QObject * parent);
    virtual ~GraphModel();

    int rowCount(const QModelIndex &parent = QModelIndex()) const override;
    QHash<int, QByteArray> roleNames() const override;
    QVariant data(const QModelIndex &index, int role) const override;

    enum Column {
        wordRole = Qt::UserRole + 1,
        countRole,
        colorRole,
        EndOfColumnRole
    };

signals:
    void doParse(const QString & fileName);
    void doFinishParsing();

public slots:
    void openFile(const QUrl &fileName);

private slots:
    void onUpdateModel(const std::vector<QPair<QString, qint32>> &sorted, const qint32 wordsCount);

private:
    std::vector<QSharedPointer<BarInfo>> m_rows;

    QThread m_thread;
    QScopedPointer<Parser> m_parser;
};

#endif // GRAPHMODEL_H
